package com.fexco.util;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fexco.constant.Constants;

/**
 * Utility class
 * 
 * @author guimachado
 *
 */
public class Util {
	
	  /**
	   * Generate hash md5 from String.
	   * 
	   * @param var String
	   * @return String
	   * @throws Exception
	   */
	  public static String md5(String var)throws Exception {
		  
	        MessageDigest md = MessageDigest.getInstance("MD5");
	        md.update(var.getBytes());

	        byte byteData[] = md.digest();

	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }

	        StringBuffer hexString = new StringBuffer();
	    	for (int i=0;i<byteData.length;i++) {
	    		String hex=Integer.toHexString(0xff & byteData[i]);
	   	     	if(hex.length()==1) hexString.append('0');
	   	     	hexString.append(hex);
	    	}
	    	return hexString.toString();
	    }
	  
	  
	  public static Map<String, String> getRequestParameters(HttpServletRequest request){
			
			String [] parameters = Constants.QUERY_PARAMETERS; 
			
			Map<String, String> map = new HashMap<>();
			map.put("format", "xml");
			
			for(String param : parameters){
				if(request.getParameter(param) != null){
					map.put(param, request.getParameter(param));
				}
			}		
			return map;		
		}
	  
}
