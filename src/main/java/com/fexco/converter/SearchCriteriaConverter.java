package com.fexco.converter;

import java.util.Map;

import org.apache.log4j.Logger;

import com.fexco.constant.Constants;
import com.fexco.constant.SearchType;
import com.fexco.entity.SearchCriteria;
import com.fexco.exception.SystemException;
import com.fexco.util.Util;

public class SearchCriteriaConverter {

	final static Logger logger = Logger.getLogger(SearchCriteriaConverter.class);

	/*
	 * Prepare search criteria request.
	 */
	public static SearchCriteria formatParamToSearchCriteria(SearchType searchType, String postCode, String pcwCode,
			Map<String, String> params) throws SystemException {

		SearchCriteria searchCriteria = new SearchCriteria(pcwCode, postCode, searchType);

		for (Map.Entry<String, String> entry : params.entrySet()) {

			switch (entry.getKey()) {
			case Constants.ADD_TAGS:
				searchCriteria.setAddtags(entry.getValue());
				break;

			case Constants.INCLUDE:
				searchCriteria.setInclude(entry.getValue());
				break;

			case Constants.EXCLUDE:
				searchCriteria.setExclude(entry.getValue());
				break;

			case Constants.PAGE:
				searchCriteria.setPage(entry.getValue());
				break;

			case Constants.CALLBACK:
				searchCriteria.setCallback(entry.getValue());
				break;

			case Constants.LINES:
				searchCriteria.setLines(entry.getValue());
				break;

			case Constants.POSTCODEONLY:
				searchCriteria.setPostcodeonly(entry.getValue());
				break;

			case Constants.IDENTIFIER:
				searchCriteria.setAddtags(entry.getValue());
				break;

			case Constants.DISTANCE:
				searchCriteria.setDistance(entry.getValue());
				break;

			default:
				break;
			}
		}

		// generate the md5 hash as key
		try {
			searchCriteria.setKey(Util.md5(searchCriteria.toString()));
		} catch (Exception e) {
			logger.error(Constants.MD5_ERROR, e);
			throw new SystemException(Constants.MD5_ERROR);
		}

		searchCriteria.setMap(params);

		return searchCriteria;
	}

	/**
	 * 
	 * Prepare search criteria request.
	 *
	 * 
	 * @param searchType
	 * @param pcwCode
	 * @param latitude
	 * @param longitude
	 * @param params
	 * @return SearchCriteria
	 * @throws SystemException
	 */
	public static SearchCriteria formatParamToSearchCriteria(SearchType searchType, String pcwCode, String latitude,
			String longitude, Map<String, String> params) throws SystemException {

		SearchCriteria searchCriteria = formatParamToSearchCriteria(searchType, null, pcwCode, params);
		searchCriteria.setLatitude(latitude);
		searchCriteria.setLongitude(longitude);

		// overwrite after changes the md5 hash as key
		try {
			searchCriteria.setKey(Util.md5(searchCriteria.toString()));
		} catch (Exception e) {
			logger.error(Constants.MD5_ERROR, e);
			throw new SystemException(Constants.MD5_ERROR);
		}
		return searchCriteria;

	}

}
