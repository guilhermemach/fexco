package com.fexco.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import com.fexco.entity.SearchCriteria;

/**
 * Repository for SearchCriteria
 * 
 * @author Guilherme
 *
 */
public interface SearchCriteriaRepository extends JpaRepository<SearchCriteria, Integer> {	
	

}

