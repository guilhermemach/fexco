package com.fexco.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fexco.constant.SearchType;
import com.fexco.entity.Address;

/**
 * Repository for Address
 * 
 * @author Guilherme
 *
 */
public interface AddressRepository extends JpaRepository<Address, Integer> {	
	
	public List<Address> findByCriteria(SearchType searchType, String key);
	
}

