package com.fexco.filter;

import javax.servlet.*;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Request interceptor.
 * @author gmachado
 *
 */
@Component
public class FilterWrapper implements Filter {
	FilterConfig config = null;
	final static Logger logger = Logger.getLogger(FilterWrapper.class);

	public void init(FilterConfig config) {
		this.config = config;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) {
		RequestWrap request = new RequestWrap(req);
		try {
			chain.doFilter(request, res);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void destroy() {
	}
}
