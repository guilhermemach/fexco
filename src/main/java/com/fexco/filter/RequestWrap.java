package com.fexco.filter;

import javax.servlet.ServletRequest;
import javax.servlet.http.*;

import org.springframework.http.MediaType;

import com.fexco.constant.Constants;

public class RequestWrap extends HttpServletRequestWrapper {

	public HttpServletRequest request;

	public RequestWrap(ServletRequest request) {
		super((HttpServletRequest) request);
		this.request = (HttpServletRequest) request;
	}

	/* Overloading the getHeader method */
	public String getHeader(String name) {
		String value = null;
		if ("Accept".equals(name) && request.getParameter(Constants.FORMAT) != null) {
			value = super.getHeader(name);
			String format = request.getParameter(Constants.FORMAT);
			if (format.equals(Constants.XML) || format.equals(Constants.JSON)) {
				value = "application/" + format;
			} else {				
				value = super.getHeader(name);				
			}
		} else {
			value = super.getHeader(name);
			if(value == null || (!value.equals(MediaType.APPLICATION_JSON_VALUE) && !value.equals(MediaType.APPLICATION_XML_VALUE))){
				value = MediaType.APPLICATION_JSON_VALUE;
			}
		}
		return value;
	}
}
