package com.fexco.exception;

/**
 * Especialized exception.
 * 
 * @author Guilherme
 *
 */
public class SystemException extends Exception {

	private static final long serialVersionUID = 1L;
	private String msg;
	private Integer code;

	public SystemException(String msg) {
		super(msg);
		this.msg = msg;
	}
	
	public SystemException(String msg, Integer code) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}

	@Override
	public String getMessage() {
		return msg;
	}

	public Integer getCode() {
		return code;
	}
	
}
