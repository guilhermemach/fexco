package com.fexco.retrofit;

import java.util.Map;

import com.fexco.entity.AddressGroup;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Client http call interface.
 * 
 * @author gmachado
 *
 */
public interface RetrofitAPI {

	@GET("pcw/{api-key}/address/ie/{eir-code}")
	Call<AddressGroup> getAddress(@Path("api-key") String apikey, @Path("eir-code") String eircode ,@QueryMap Map<String, String> params);
	
	@GET("pcw/{api-key}/addressgeo/ie/{eir-code}")
	Call<AddressGroup> getAddressGeo(@Path("api-key") String apikey, @Path("eir-code") String eircode ,@QueryMap Map<String, String> params);	
	
	@GET("pcw/{api-key}/position/ie/{eir-code}")
	Call<AddressGroup> getPosition(@Path("api-key") String apikey, @Path("eir-code") String eircode ,@QueryMap Map<String, String> params);
	
	@GET("pcw/{api-key}/rgeo/ie/{latitude}/{longitude}")
	Call<AddressGroup> getReverseGeoCode(@Path("api-key") String apikey, @Path("latitude") String latitude, @Path("longitude") String longitude ,@QueryMap Map<String, String> params);

}