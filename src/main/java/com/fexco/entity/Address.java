package com.fexco.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;


@NamedQuery(name = "Address.findByCriteria", query = "SELECT a FROM Address a join a.criteria c WHERE  c.searchType = ?1 and c.key = ?2")
@Entity
@Root(name = "Address")
public class Address implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer id;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "addressline1", required = false)
	private String addressline1;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "addressline2", required = false)
	private String addressline2;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "addressline3", required = false)
	private String addressline3;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "summaryline", required = false)
	private String summaryline;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "organisation", required = false)
	private String organisation;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "departmentname", required = false)
	private String departmentName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "buildingname", required = false)
	private String buildingname;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "subbuildingname", required = false)
	private String subbuildingName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "dependentstreet", required = false)
	private String dependentStreet;
		
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "buildinggroupname", required = false)
	private String buildinggroupname;
		
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "doubledependentlocality", required = false)
	private String doubleDependentLocality;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "dependentlocality", required = false)
	private String dependentLocality;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "recodes", required = false)
	private String recodes;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "morevalues", required = false)
	private Boolean moreValues;
		
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "nextpage", required = false)
	private Integer nextpage;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "alias", required = false)
	private Boolean alias;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "totalresults", required = false)
	private Integer totalResults;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "number", required = false)
	private String number;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "premise", required = false)
	private String premise;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "street", required = false)
	private String street;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "posttown", required = false)
	private String posttown;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "county", required = false)
	private String county;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "postcode", required = false)
	private String postcode;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "latitude", required = false)
	private String latitude;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "longitude", required = false)
	private String longitude;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "what3words", required = false)
	private String what3words;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "pobox", required = false)
	private String pobox;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "deliverypointsuffix", required = false)
	private String deliveryPointSuffix;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "uniquedeliverypointreferencenumber", required = false)
	private String uniqueDeliveryPointReferenceNumber;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "grideasting", required = false)
	private String grideasting;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "gridnorthing", required = false)
	private String gridnorthing;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "latitudeosgb", required = false)
	private String latitudeosGb;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "longitudeosgb", required = false)
	private String longitudeosGb;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "postcodedateofintroduction", required = false)
	private String postCodeDateOfIntroduction;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "nationalparkcode", required = false)
	private String nationalParkCode;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "nationalparkname", required = false)
	private String nationalParkName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "localauthoritywardcode", required = false)
	private String localAuthoritywardCode;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "localauthoritycode", required = false)
	private String localAuthorityCode;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "localauthorityname", required = false)
	private String localAuthorityName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "electoralwardcode", required = false)
	private String electoralwardCode;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "electoralwardname", required = false)
	private String electoralwardName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "electoralcountycode", required = false)
	private String electoralCountyCode;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "electoralcountyname", required = false)
	private String electoralCountyName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "parliamentaryconstituencycode", required = false)
	private String parliamentaryConstituencyCode;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "parliamentaryconstituencyname", required = false)
	private String parliamentaryConstituencyName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "europeanelectoralregioncode", required = false)
	private String europeanElectoralRegioncode;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "europeanelectoralregionname", required = false)
	private String europeanElectoralRegionname;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "commissioningregioncode_gss", required = false)
	private String commissioningRegioncodeGss;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "commissioningregionname", required = false)
	private String commissioningRegionName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "areateamcode_nhs", required = false)
	private String areateamCodeNhs;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "areateamcode_gss", required = false)
	private String areateamCodeGss;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "areateamname", required = false)
	private String areateamName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "clinicalcommissioninggroupcode_nhs", required = false)
	private String clinicalCommissioningGroupcodeNhs;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "clinicalcommissioninggroupcode_gss", required = false)
	private String clinicalCommissioningGroupCodeGss;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "clinicalcommissioninggroupname", required = false)
	private String clinicalCommissionInggroupName;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "oa11", required = false)
	private String oa11;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "lsoa11", required = false)
	private String lsoa11;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Element(name = "msoa11", required = false)
	private String msoa11;
	
	@ManyToOne
	@JoinColumn(name = "criteria", nullable=false, updatable=false)
	@JsonIgnore
	private SearchCriteria criteria;

	public String getAddressline1() {
		return addressline1;
	}

	public void setAddressline1(String addressline1) {
		this.addressline1 = addressline1;
	}

	public String getAddressline2() {
		return addressline2;
	}

	public void setAddressline2(String addressline2) {
		this.addressline2 = addressline2;
	}

	public String getSummaryline() {
		return summaryline;
	}

	public void setSummaryline(String summaryline) {
		this.summaryline = summaryline;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPremise() {
		return premise;
	}

	public void setPremise(String premise) {
		this.premise = premise;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPosttown() {
		return posttown;
	}

	public void setPosttown(String posttown) {
		this.posttown = posttown;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getWhat3words() {
		return what3words;
	}

	public void setWhat3words(String what3words) {
		this.what3words = what3words;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SearchCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(SearchCriteria criteria) {
		this.criteria = criteria;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddressline3() {
		return addressline3;
	}

	public void setAddressline3(String addressline3) {
		this.addressline3 = addressline3;
	}

	public String getBuildingname() {
		return buildingname;
	}

	public void setBuildingname(String buildingname) {
		this.buildingname = buildingname;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getSubbuildingName() {
		return subbuildingName;
	}

	public void setSubbuildingName(String subbuildingName) {
		this.subbuildingName = subbuildingName;
	}

	public String getDependentStreet() {
		return dependentStreet;
	}

	public void setDependentStreet(String dependentStreet) {
		this.dependentStreet = dependentStreet;
	}

	public String getDoubleDependentLocality() {
		return doubleDependentLocality;
	}

	public void setDoubleDependentLocality(String doubleDependentLocality) {
		this.doubleDependentLocality = doubleDependentLocality;
	}

	public String getDependentLocality() {
		return dependentLocality;
	}

	public void setDependentLocality(String dependentLocality) {
		this.dependentLocality = dependentLocality;
	}

	public String getRecodes() {
		return recodes;
	}

	public void setRecodes(String recodes) {
		this.recodes = recodes;
	}

	public Boolean getMoreValues() {
		return moreValues;
	}

	public void setMoreValues(Boolean moreValues) {
		this.moreValues = moreValues;
	}

	public Integer getNextpage() {
		return nextpage;
	}

	public void setNextpage(Integer nextpage) {
		this.nextpage = nextpage;
	}

	public Integer getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(Integer totalResults) {
		this.totalResults = totalResults;
	}

	public String getBuildinggroupname() {
		return buildinggroupname;
	}

	public void setBuildinggroupname(String buildinggroupname) {
		this.buildinggroupname = buildinggroupname;
	}

	public String getPobox() {
		return pobox;
	}

	public void setPobox(String pobox) {
		this.pobox = pobox;
	}

	public Boolean getAlias() {
		return alias;
	}

	public void setAlias(Boolean alias) {
		this.alias = alias;
	}

	public String getDeliveryPointSuffix() {
		return deliveryPointSuffix;
	}

	public void setDeliveryPointSuffix(String deliveryPointSuffix) {
		this.deliveryPointSuffix = deliveryPointSuffix;
	}

	public String getUniqueDeliveryPointReferenceNumber() {
		return uniqueDeliveryPointReferenceNumber;
	}

	public void setUniqueDeliveryPointReferenceNumber(String uniqueDeliveryPointReferenceNumber) {
		this.uniqueDeliveryPointReferenceNumber = uniqueDeliveryPointReferenceNumber;
	}

	public String getGrideasting() {
		return grideasting;
	}

	public void setGrideasting(String grideasting) {
		this.grideasting = grideasting;
	}

	public String getGridnorthing() {
		return gridnorthing;
	}

	public void setGridnorthing(String gridnorthing) {
		this.gridnorthing = gridnorthing;
	}

	public String getLatitudeosGb() {
		return latitudeosGb;
	}

	public void setLatitudeosGb(String latitudeosGb) {
		this.latitudeosGb = latitudeosGb;
	}

	public String getLongitudeosGb() {
		return longitudeosGb;
	}

	public void setLongitudeosGb(String longitudeosGb) {
		this.longitudeosGb = longitudeosGb;
	}

	public String getPostCodeDateOfIntroduction() {
		return postCodeDateOfIntroduction;
	}

	public void setPostCodeDateOfIntroduction(String postCodeDateOfIntroduction) {
		this.postCodeDateOfIntroduction = postCodeDateOfIntroduction;
	}

	public String getNationalParkCode() {
		return nationalParkCode;
	}

	public void setNationalParkCode(String nationalParkCode) {
		this.nationalParkCode = nationalParkCode;
	}

	public String getNationalParkName() {
		return nationalParkName;
	}

	public void setNationalParkName(String nationalParkName) {
		this.nationalParkName = nationalParkName;
	}

	public String getLocalAuthoritywardCode() {
		return localAuthoritywardCode;
	}

	public void setLocalAuthoritywardCode(String localAuthoritywardCode) {
		this.localAuthoritywardCode = localAuthoritywardCode;
	}

	public String getLocalAuthorityCode() {
		return localAuthorityCode;
	}

	public void setLocalAuthorityCode(String localAuthorityCode) {
		this.localAuthorityCode = localAuthorityCode;
	}

	public String getLocalAuthorityName() {
		return localAuthorityName;
	}

	public void setLocalAuthorityName(String localAuthorityName) {
		this.localAuthorityName = localAuthorityName;
	}

	public String getElectoralwardCode() {
		return electoralwardCode;
	}

	public void setElectoralwardCode(String electoralwardCode) {
		this.electoralwardCode = electoralwardCode;
	}

	public String getElectoralwardName() {
		return electoralwardName;
	}

	public void setElectoralwardName(String electoralwardName) {
		this.electoralwardName = electoralwardName;
	}

	public String getElectoralCountyCode() {
		return electoralCountyCode;
	}

	public void setElectoralCountyCode(String electoralCountyCode) {
		this.electoralCountyCode = electoralCountyCode;
	}

	public String getElectoralCountyName() {
		return electoralCountyName;
	}

	public void setElectoralCountyName(String electoralCountyName) {
		this.electoralCountyName = electoralCountyName;
	}

	public String getParliamentaryConstituencyCode() {
		return parliamentaryConstituencyCode;
	}

	public void setParliamentaryConstituencyCode(String parliamentaryConstituencyCode) {
		this.parliamentaryConstituencyCode = parliamentaryConstituencyCode;
	}

	public String getParliamentaryConstituencyName() {
		return parliamentaryConstituencyName;
	}

	public void setParliamentaryConstituencyName(String parliamentaryConstituencyName) {
		this.parliamentaryConstituencyName = parliamentaryConstituencyName;
	}

	public String getEuropeanElectoralRegioncode() {
		return europeanElectoralRegioncode;
	}

	public void setEuropeanElectoralRegioncode(String europeanElectoralRegioncode) {
		this.europeanElectoralRegioncode = europeanElectoralRegioncode;
	}

	public String getEuropeanElectoralRegionname() {
		return europeanElectoralRegionname;
	}

	public void setEuropeanElectoralRegionname(String europeanElectoralRegionname) {
		this.europeanElectoralRegionname = europeanElectoralRegionname;
	}

	public String getCommissioningRegioncodeGss() {
		return commissioningRegioncodeGss;
	}

	public void setCommissioningRegioncodeGss(String commissioningRegioncodeGss) {
		this.commissioningRegioncodeGss = commissioningRegioncodeGss;
	}

	public String getCommissioningRegionName() {
		return commissioningRegionName;
	}

	public void setCommissioningRegionName(String commissioningRegionName) {
		this.commissioningRegionName = commissioningRegionName;
	}

	public String getAreateamCodeNhs() {
		return areateamCodeNhs;
	}

	public void setAreateamCodeNhs(String areateamCodeNhs) {
		this.areateamCodeNhs = areateamCodeNhs;
	}

	public String getAreateamCodeGss() {
		return areateamCodeGss;
	}

	public void setAreateamCodeGss(String areateamCodeGss) {
		this.areateamCodeGss = areateamCodeGss;
	}

	public String getAreateamName() {
		return areateamName;
	}

	public void setAreateamName(String areateamName) {
		this.areateamName = areateamName;
	}

	public String getClinicalCommissioningGroupcodeNhs() {
		return clinicalCommissioningGroupcodeNhs;
	}

	public void setClinicalCommissioningGroupcodeNhs(String clinicalCommissioningGroupcodeNhs) {
		this.clinicalCommissioningGroupcodeNhs = clinicalCommissioningGroupcodeNhs;
	}

	public String getClinicalCommissioningGroupCodeGss() {
		return clinicalCommissioningGroupCodeGss;
	}

	public void setClinicalCommissioningGroupCodeGss(String clinicalCommissioningGroupCodeGss) {
		this.clinicalCommissioningGroupCodeGss = clinicalCommissioningGroupCodeGss;
	}

	public String getClinicalCommissionInggroupName() {
		return clinicalCommissionInggroupName;
	}

	public void setClinicalCommissionInggroupName(String clinicalCommissionInggroupName) {
		this.clinicalCommissionInggroupName = clinicalCommissionInggroupName;
	}

	public String getOa11() {
		return oa11;
	}

	public void setOa11(String oa11) {
		this.oa11 = oa11;
	}

	public String getLsoa11() {
		return lsoa11;
	}

	public void setLsoa11(String lsoa11) {
		this.lsoa11 = lsoa11;
	}

	public String getMsoa11() {
		return msoa11;
	}

	public void setMsoa11(String msoa11) {
		this.msoa11 = msoa11;
	}
}