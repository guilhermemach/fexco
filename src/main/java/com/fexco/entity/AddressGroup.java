package com.fexco.entity;

import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@Root(name="Addresses", strict=false)
@JacksonXmlRootElement(localName = "Addresses")
public class AddressGroup {
	
	@JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Address", isAttribute = false)
	@ElementList(name="Address",inline=true)
	private List<Address> address;
	
	public AddressGroup(){}
	
	public AddressGroup(List<Address> address){ this.address = address;}
	
	public List<Address> getAddress() {
		return address;
	}
	
	public void setAddress(List<Address> address) {
		this.address = address;
	}

}
