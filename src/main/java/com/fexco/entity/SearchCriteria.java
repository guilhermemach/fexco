package com.fexco.entity;

import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fexco.constant.SearchType;

@Entity
public class SearchCriteria {	
		
	public SearchCriteria(String pcwCode, String postCode, SearchType searchType) {
		super();
		this.pcwCode = pcwCode;
		this.postCode = postCode;
		this.searchType = searchType;
	}
	
	public SearchCriteria(String pcwCode, String lines, SearchType searchType, String latitude, String longitude, String distance) {
		super();
		this.pcwCode = pcwCode;
		this.lines = lines;
		this.searchType = searchType;
		this.latitude = latitude;
		this.longitude = longitude;
		this.distance = distance;
	}
	
	public SearchCriteria(){};
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer id;
	private String pcwCode;
	private String postCode;
	private String lines;
	private SearchType searchType;
	private String latitude;
	private String longitude;
	private String distance;
	private String key;
	private String include;
	private String exclude;
	private String postcodeonly;
	private String page;
	private String addtags;
	private String identifier;
	private String callback;
	private String alias;
	@Transient
	private Map<String, String> map;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity=Address.class)
	private List<Address> addresses;
	
	public String getPcwCode() {
		return pcwCode;
	}
	public void setPcwCode(String pcwCode) {
		this.pcwCode = pcwCode;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getLines() {
		return lines;
	}
	public void setLines(String lines) {
		this.lines = lines;
	}
	public SearchType getSearchType() {
		return searchType;
	}
	public void setSearchType(SearchType searchType) {
		this.searchType = searchType;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPostcodeonly() {
		return postcodeonly;
	}

	public void setPostcodeonly(String postcodeonly) {
		this.postcodeonly = postcodeonly;
	}

	public String getInclude() {
		return include;
	}

	public void setInclude(String include) {
		this.include = include;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getAddtags() {
		return addtags;
	}

	public void setAddtags(String addtags) {
		this.addtags = addtags;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}	

	public String getExclude() {
		return exclude;
	}

	public void setExclude(String exclude) {
		this.exclude = exclude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addresses == null) ? 0 : addresses.hashCode());
		result = prime * result + ((addtags == null) ? 0 : addtags.hashCode());
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((callback == null) ? 0 : callback.hashCode());
		result = prime * result + ((distance == null) ? 0 : distance.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result + ((include == null) ? 0 : include.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((getLines() == null) ? 0 : getLines().hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		result = prime * result + ((page == null) ? 0 : page.hashCode());
		result = prime * result + ((pcwCode == null) ? 0 : pcwCode.hashCode());
		result = prime * result + ((postCode == null) ? 0 : postCode.hashCode());
		result = prime * result + ((postcodeonly == null) ? 0 : postcodeonly.hashCode());
		result = prime * result + ((searchType == null) ? 0 : searchType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (addresses == null) {
			if (other.addresses != null)
				return false;
		} else if (!addresses.equals(other.addresses))
			return false;
		if (addtags == null) {
			if (other.addtags != null)
				return false;
		} else if (!addtags.equals(other.addtags))
			return false;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (callback == null) {
			if (other.callback != null)
				return false;
		} else if (!callback.equals(other.callback))
			return false;
		if (distance == null) {
			if (other.distance != null)
				return false;
		} else if (!distance.equals(other.distance))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (identifier == null) {
			if (other.identifier != null)
				return false;
		} else if (!identifier.equals(other.identifier))
			return false;
		if (include == null) {
			if (other.include != null)
				return false;
		} else if (!include.equals(other.include))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (getLines() == null) {
			if (other.getLines() != null)
				return false;
		} else if (!getLines().equals(other.getLines()))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		if (page == null) {
			if (other.page != null)
				return false;
		} else if (!page.equals(other.page))
			return false;
		if (pcwCode == null) {
			if (other.pcwCode != null)
				return false;
		} else if (!pcwCode.equals(other.pcwCode))
			return false;
		if (postCode == null) {
			if (other.postCode != null)
				return false;
		} else if (!postCode.equals(other.postCode))
			return false;
		if (postcodeonly == null) {
			if (other.postcodeonly != null)
				return false;
		} else if (!postcodeonly.equals(other.postcodeonly))
			return false;
		if (searchType != other.searchType)
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		return "SearchCriteria [pcwCode=" + pcwCode + ", postCode=" + postCode + ", lines=" + getLines()
				+ ", searchType=" + searchType + ", latitude=" + latitude + ", longitude=" + longitude + ", distance="
				+ distance + ", include=" + include + ", postcodeonly=" + postcodeonly
				+ ", page=" + page + ", addtags=" + addtags + ", identifier=" + identifier + ", callback=" + callback
				+ ", alias=" + alias + ", addresses=" + addresses + "]";
	}
	
	
}
