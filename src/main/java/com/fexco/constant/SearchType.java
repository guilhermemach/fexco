package com.fexco.constant;

public enum SearchType {

	ADDRESS, ADDRESS_GEO, POSITION, REVERSE_GEO;

}
