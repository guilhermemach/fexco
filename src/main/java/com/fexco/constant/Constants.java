package com.fexco.constant;

/**
 * General constants.
 * 
 * @author Guilherme
 *
 */
public abstract class Constants {
	
	public static final String XML = "xml";
	public static final String JSON = "json";
	public static final String FORMAT = "format";
	
	// error 
	public static final String THIRD_PART_ERROR = "Third part error.";
	public static final String MD5_ERROR = "MD5 error.";
	public static final String DATABASE_ERROR = "Database error.";
	
	public static final String[] QUERY_PARAMETERS = {"lines","include","exclude","addtags","identifier","callback","page","postcodeonly","alias","distance"};
	public static final String LINES = "lines";
	public static final String INCLUDE = "include";
	public static final String EXCLUDE = "exclude";
	public static final String ADD_TAGS = "addtags";
	public static final String IDENTIFIER = "identifier";
	public static final String CALLBACK = "callback";
	public static final String PAGE = "page";
	public static final String POSTCODEONLY = "postcodeonly";
	public static final String ALIAS = "alias";
	public static final String DISTANCE = "distance";
}
