package com.fexco.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fexco.constant.SearchType;
import com.fexco.converter.SearchCriteriaConverter;
import com.fexco.entity.Address;
import com.fexco.entity.AddressGroup;
import com.fexco.entity.SearchCriteria;
import com.fexco.exception.SystemException;
import com.fexco.service.AddressService;
import com.fexco.util.Util;

/**
 * Rest controller to get adress.
 * 
 * @author Guilherme
 *
 */
@RestController
public class AddressController {

	@Autowired
	private AddressService service;

	/**
	 * Address call.
	 * 
	 * @return ResponseEntity
	 */
	@RequestMapping(value = "/pcw/{pcw_code}/address/ie/{post_code}", method = RequestMethod.GET)
	public ResponseEntity<Object> getAddress(@PathVariable("pcw_code") String pcwCode,
			@PathVariable("post_code") String postCode,
			HttpServletRequest request) {

		try{
			SearchCriteria objRequest = SearchCriteriaConverter.formatParamToSearchCriteria(SearchType.ADDRESS, postCode, pcwCode, Util.getRequestParameters(request));
			
			List<Address> addresses = service.getAddresses(objRequest);
			return sucessResponse(new AddressGroup(addresses), request);
			
		}catch (SystemException e) {
			return new ResponseEntity<Object>(e.getMessage(), e.getCode() == null ? HttpStatus.SERVICE_UNAVAILABLE : HttpStatus.valueOf(e.getCode()));
		}		
	}

	/**
	 * Address Geo call.
	 * 
	 * @return ResponseEntity
	 */
	@RequestMapping(value = "/pcw/{pcw_code}/addressgeo/ie/{post_code}", method = RequestMethod.GET)
	public ResponseEntity<Object> getAddressPosition(@PathVariable("pcw_code") String pcwCode,
			@PathVariable("post_code") String postCode, @RequestParam(value = "lines", required = false) String lines,
			HttpServletRequest request) {
		
		try{
			SearchCriteria objRequest = SearchCriteriaConverter.formatParamToSearchCriteria(SearchType.ADDRESS_GEO, postCode, pcwCode, Util.getRequestParameters(request));

			List<Address> addresses = service.getAddresses(objRequest);
			return sucessResponse(new AddressGroup(addresses), request);
		}catch (SystemException e) {
			return new ResponseEntity<Object>(e.getMessage(), e.getCode() == null ? HttpStatus.SERVICE_UNAVAILABLE : HttpStatus.valueOf(e.getCode()));
		}
	}

	/**
	 * Position call.
	 * 
	 * @return ResponseEntity
	 */
	@RequestMapping(value = "/pcw/{pcw_code}/position/ie/{post_code}", method = RequestMethod.GET)
	public ResponseEntity<Object> getPositions(@PathVariable("pcw_code") String pcwCode,
			@PathVariable("post_code") String postCode,
			HttpServletRequest request) {
		
		try{
			SearchCriteria objRequest = SearchCriteriaConverter.formatParamToSearchCriteria(SearchType.POSITION, postCode, pcwCode, Util.getRequestParameters(request));

			List<Address> addresses = service.getAddresses(objRequest);
			return sucessResponse(new AddressGroup(addresses), request);
		}catch (SystemException e) {
			return new ResponseEntity<Object>(e.getMessage(), e.getCode() == null ? HttpStatus.SERVICE_UNAVAILABLE : HttpStatus.valueOf(e.getCode()));
		}
	}
	
	/**
	 * Address Reverse Geo call.
	 * 
	 * @return ResponseEntity
	 */
	@RequestMapping(value = "/pcw/{pcw_code}/rgeo/ie/{latitude:.+}/{longitude:.+}", method = RequestMethod.GET)
	public ResponseEntity<Object> getReverseGeocodeAddresses (@PathVariable("pcw_code") String pcwCode,
			@PathVariable("latitude") String latitude, @PathVariable("longitude") String longitude, 
			HttpServletRequest request) {
		
		try{
			SearchCriteria objRequest = SearchCriteriaConverter.formatParamToSearchCriteria(SearchType.REVERSE_GEO, pcwCode, latitude, longitude, Util.getRequestParameters(request));
			
			List<Address> addresses = service.getAddresses(objRequest);
			return sucessResponse(new AddressGroup(addresses), request);
		}catch (SystemException e) {
			return new ResponseEntity<Object>(e.getMessage(), e.getCode() == null ? HttpStatus.SERVICE_UNAVAILABLE : HttpStatus.valueOf(e.getCode()));
		}
	}
	
	/**
	 * Method to format the response.
	 * @param group AddressGroup
	 * @param request HttpServletRequest
	 * @return ResponseEntity
	 */
	private ResponseEntity<Object> sucessResponse(AddressGroup group, HttpServletRequest request) {

		if (request.getHeader("Accept").equals(MediaType.APPLICATION_JSON_VALUE)) {
			return new ResponseEntity<Object>(group.getAddress(), HttpStatus.OK);
		}

		return new ResponseEntity<Object>(group, HttpStatus.OK);
	}	
	

}