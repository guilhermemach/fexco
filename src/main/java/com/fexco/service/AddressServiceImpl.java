package com.fexco.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fexco.constant.Constants;
import com.fexco.entity.Address;
import com.fexco.entity.SearchCriteria;
import com.fexco.exception.SystemException;
import com.fexco.repositories.AddressRepository;
import com.fexco.repositories.SearchCriteriaRepository;
import com.fexco.util.Util;

/**
 * Addrress service.
 * 
 * @author guimachado
 *
 */
@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressRepository repository;

	@Autowired
	private SearchCriteriaRepository searchRepository;

	@Autowired
	private PostCoderWebService thirdPartApiService;

	final static Logger logger = Logger.getLogger(AddressServiceImpl.class);

	/**
	 * Generic address call. the criteria redirect to the correct final call.
	 * @throws SystemException 
	 */
	public List<Address> getAddresses(SearchCriteria searchCriteriaRequest) throws SystemException {

		logger.info("getAddresses starting.");

		List<Address> addresses = null;
		try {
			addresses = this.repository.findByCriteria(searchCriteriaRequest.getSearchType(), Util.md5(searchCriteriaRequest.toString()));
		} catch (Exception e) {
			logger.error(Constants.DATABASE_ERROR, e);
			throw new SystemException(Constants.DATABASE_ERROR);			
		}
		
		if (addresses.isEmpty()) {

			logger.info("getAddresses calling third part API.");

			addresses = this.thirdPartApiService.getGenericAddress(searchCriteriaRequest).getAddress();

			if (!addresses.isEmpty()) {
				logger.info("getAddresses saving the data.");

				this.searchRepository.save(searchCriteriaRequest);

				prepareAddress(addresses, searchCriteriaRequest);
				this.repository.save(addresses);
			}
		}

		logger.info("getAddresses finishing");

		return addresses;
	}

	
	/**
	 * Dependency preparation.
	 * 
	 * @param list - List<Address>
	 * @param criteria - SearchCriteria
	 */
	private void prepareAddress(List<Address> list, SearchCriteria criteria) {
		list.forEach((address) -> address.setCriteria(criteria));
	}

}
