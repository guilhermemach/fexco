package com.fexco.service;

import java.util.List;

import com.fexco.entity.Address;
import com.fexco.entity.SearchCriteria;
import com.fexco.exception.SystemException;

public interface AddressService {

	public List<Address> getAddresses(SearchCriteria vo) throws SystemException ;
	
}
