package com.fexco.service;


import com.fexco.entity.AddressGroup;
import com.fexco.entity.SearchCriteria;
import com.fexco.exception.SystemException;

public interface PostCoderWebService {
	
	public AddressGroup getGenericAddress(SearchCriteria searchCriteria) throws SystemException ;
	
}