package com.fexco.service;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fexco.constant.Constants;
import com.fexco.entity.AddressGroup;
import com.fexco.entity.SearchCriteria;
import com.fexco.exception.SystemException;
import com.fexco.retrofit.RetrofitAPI;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Third part class comunication.
 * 
 * @author guimachado
 *
 */
@Service
public class PostCoderWebServiceImpl implements PostCoderWebService {

	final String BASE_URL = "http://ws.postcoder.com/";
	final OkHttpClient.Builder client = new OkHttpClient.Builder();
	
	final static Logger logger = Logger.getLogger(PostCoderWebServiceImpl.class);

	
	/**
	 * Generic call for all operations to third party.
	 */
	public AddressGroup getGenericAddress(SearchCriteria searchCriteria) throws SystemException {

		Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(SimpleXmlConverterFactory.create()).client(client.build()).build();

		RetrofitAPI api = retrofit.create(RetrofitAPI.class);
		
			Call<AddressGroup> response = null;
			
			switch (searchCriteria.getSearchType()) {
			case ADDRESS:
				response = api.getAddress(searchCriteria.getPcwCode(), searchCriteria.getPostCode(), searchCriteria.getMap());
				break;

			case ADDRESS_GEO:
				response = api.getAddressGeo(searchCriteria.getPcwCode(), searchCriteria.getPostCode(), searchCriteria.getMap());
				break;

			case POSITION:
				response = api.getPosition(searchCriteria.getPcwCode(), searchCriteria.getPostCode(), searchCriteria.getMap());
				break;

			case REVERSE_GEO:
				response = api.getReverseGeoCode(searchCriteria.getPcwCode(),searchCriteria.getLatitude(), searchCriteria.getLongitude(),  searchCriteria.getMap());
				break;

			default:
				break;
			}
			 
			Response<AddressGroup> listRest = null;
			try {
				listRest = response.execute();
			} catch (IOException e) {
				logger.error(e);
				throw new SystemException(Constants.THIRD_PART_ERROR);
			}

			/* Checks if something is wrong and return either the body message or the code and message error.*/
			
			if (listRest.isSuccessful()) {
				return listRest.body();
			} else{
				logger.info(listRest.message());
				logger.info("HTTP code: "+listRest.code());				
				throw new SystemException(Constants.THIRD_PART_ERROR + ": " + listRest.message(), listRest.code());
			}
		
	}
}
