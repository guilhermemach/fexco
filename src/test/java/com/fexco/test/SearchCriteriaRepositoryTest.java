package com.fexco.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fexco.Main;
import com.fexco.entity.SearchCriteria;
import com.fexco.repositories.SearchCriteriaRepository;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
public class SearchCriteriaRepositoryTest {

	@Autowired
	private SearchCriteriaRepository searchCriteriarepository;

	@Before
	public void setup() throws Exception {
		List<SearchCriteria> criteriaMockedList = new HelperDataMock().getMockSearchCriteriaList();
		searchCriteriarepository.save(criteriaMockedList);
	}

	@Test
	public void getFindAllTest() throws Exception {
		List<SearchCriteria> list = searchCriteriarepository.findAll();
		assertEquals(2, list.size());
	}
		
	@Test
	public void saveTest() throws Exception {
		SearchCriteria searchCriteria= new  HelperDataMock().getMockSearchCriteriaList().get(0);
		SearchCriteria response = searchCriteriarepository.save(searchCriteria);
		assertTrue(response.getId() > 0);
	}

	@After
	public void clearup() throws Exception {
		searchCriteriarepository.deleteAll();
	}

}
