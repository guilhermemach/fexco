package com.fexco.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fexco.Main;
import com.fexco.constant.SearchType;
import com.fexco.entity.AddressGroup;
import com.fexco.entity.SearchCriteria;
import com.fexco.repositories.AddressRepository;
import com.fexco.repositories.SearchCriteriaRepository;
import com.fexco.service.AddressService;
import com.fexco.service.PostCoderWebService;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
public class AddressServiceTest {

	@Autowired
	private SearchCriteriaRepository searchCriteriarepository;

	@Autowired
	private AddressRepository addressRepository;
	
	@Mock
	private PostCoderWebService postCodeService;
	
	@InjectMocks
	@Autowired
	private AddressService addressService;

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addressTest() throws Exception {
		SearchCriteria searchCriteria = new HelperDataMock().getSearchCriteriaWithKey(SearchType.ADDRESS);
		
		//mock
	    when(postCodeService.getGenericAddress(searchCriteria)).thenReturn(new AddressGroup(new HelperDataMock().getMockAddressList()));
		this.addressService.getAddresses(searchCriteria);
		assertEquals(2 , this.addressRepository.findByCriteria(searchCriteria.getSearchType(), searchCriteria.getKey()).size());
		verify(postCodeService, times(1)).getGenericAddress(searchCriteria);
	}
	
	@Test
	public void cachedAddressTest() throws Exception {
		// prepare cached data
		SearchCriteria searchCriteria = new HelperDataMock().getSearchCriteriaWithKey(SearchType.POSITION);
		searchCriteriarepository.save(searchCriteria);	
		addressRepository.save(new HelperDataMock().getMockAddressList(searchCriteria));
		
		//mock
	    when(postCodeService.getGenericAddress(searchCriteria)).thenReturn(new AddressGroup(new HelperDataMock().getMockAddressList(searchCriteria)));
		this.addressService.getAddresses(searchCriteria);
		assertEquals(2 , this.addressRepository.findByCriteria(searchCriteria.getSearchType(), searchCriteria.getKey()).size());
		verify(postCodeService, never()).getGenericAddress(searchCriteria);
	}
		
	
	@After
	public void clearup() throws Exception {
		addressRepository.deleteAll();
		searchCriteriarepository.deleteAll();
	}

}
