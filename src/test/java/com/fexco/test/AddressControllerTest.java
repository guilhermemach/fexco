package com.fexco.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doThrow;

import com.fexco.Main;
import com.fexco.constant.Constants;
import com.fexco.constant.SearchType;
import com.fexco.entity.Address;
import com.fexco.exception.SystemException;
import com.fexco.filter.FilterWrapper;
import com.fexco.repositories.AddressRepository;
import com.fexco.repositories.SearchCriteriaRepository;
import com.fexco.rest.AddressController;
import com.fexco.service.AddressService;

/**
 * Test class for front end operations.
 * 
 * @author guimachado
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class AddressControllerTest {
	
	private MockMvc mockMvc;

	@Autowired
	private SearchCriteriaRepository searchCriteriarepository;
	
	@Mock
	private AddressService addressService;

	@InjectMocks
	private AddressController addressController;
		
	@Autowired
	private FilterWrapper filterWrapper;
	
	@Autowired
	private AddressRepository addressRepository;	

	@Before
	public void setup() throws Exception {
		 MockitoAnnotations.initMocks(this);
		 this.mockMvc  = MockMvcBuilders.standaloneSetup(addressController).addFilters(filterWrapper).build();
	}
	
	@Test
	public void jsonAddressTest() throws Exception {
		Address address = new HelperDataMock().getMockAddressList().get(0);	
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.ADDRESS))).thenReturn(new HelperDataMock().getMockAddressList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/address/ie/D02X285?lines=3"))
		.andExpect(jsonPath("$[0].addressline1", is(address.getAddressline1())))
		.andExpect(jsonPath("$[0].addressline2", is(address.getAddressline2())))
		.andExpect(jsonPath("$[0].summaryline", is(address.getSummaryline())))
		.andExpect(jsonPath("$[0].organisation", is(address.getOrganisation())))
		.andExpect(jsonPath("$[0].number", is(address.getNumber())))
		.andExpect(jsonPath("$[0].premise", is(address.getPremise())))
		.andExpect(jsonPath("$[0].county", is(address.getCounty())))
		.andExpect(jsonPath("$[0].postcode", is(address.getPostcode())))
		.andExpect(status().isOk());
	}
	
	@Test
	public void jsonAddressGeoTest() throws Exception {
		Address address = new HelperDataMock().getMockAddressGeoList().get(0);
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.ADDRESS_GEO))).thenReturn(new HelperDataMock().getMockAddressGeoList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/addressgeo/ie/D02X285?lines=3"))
		.andExpect(jsonPath("$[0].addressline1", is(address.getAddressline1())))
		.andExpect(jsonPath("$[0].addressline2", is(address.getAddressline2())))
		.andExpect(jsonPath("$[0].summaryline", is(address.getSummaryline())))
		.andExpect(jsonPath("$[0].organisation", is(address.getOrganisation())))
		.andExpect(jsonPath("$[0].number", is(address.getNumber())))
		.andExpect(jsonPath("$[0].premise", is(address.getPremise())))
		.andExpect(jsonPath("$[0].county", is(address.getCounty())))
		.andExpect(jsonPath("$[0].postcode", is(address.getPostcode())))
		.andExpect(jsonPath("$[0].latitude", is(address.getLatitude())))
		.andExpect(jsonPath("$[0].longitude", is(address.getLongitude())))
		.andExpect(status().isOk());
	}
	
	@Test
	public void jsonReverseGeoTest() throws Exception {
		Address address = new HelperDataMock().getMockAddressList().get(0);
		// mock	
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaReverseGeo())).thenReturn(new HelperDataMock().getMockAddressList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/rgeo/ie/53.332067/-6.255492?distance=50"))
		.andExpect(jsonPath("$[0].addressline1", is(address.getAddressline1())))
		.andExpect(jsonPath("$[0].addressline2", is(address.getAddressline2())))
		.andExpect(jsonPath("$[0].summaryline", is(address.getSummaryline())))
		.andExpect(jsonPath("$[0].organisation", is(address.getOrganisation())))
		.andExpect(jsonPath("$[0].number", is(address.getNumber())))
		.andExpect(jsonPath("$[0].premise", is(address.getPremise())))
		.andExpect(jsonPath("$[0].county", is(address.getCounty())))
		.andExpect(jsonPath("$[0].postcode", is(address.getPostcode())))
		.andExpect(status().isOk());
	}	

	@Test
	public void jsonPositionTest() throws Exception {		
		Address address = new HelperDataMock().getMockPositionList().get(0);
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.POSITION))).thenReturn(new HelperDataMock().getMockPositionList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/position/ie/D02X285?lines=3"))
		.andExpect(jsonPath("$[0].latitude", is(address.getLatitude())))
		.andExpect(jsonPath("$[0].longitude", is(address.getLongitude())))
		.andExpect(status().isOk());
	}	

	@Test
	public void xmlQueryFormatAddressTest() throws Exception {
		Address address = new HelperDataMock().getMockAddressList().get(0);	
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.ADDRESS))).thenReturn(new HelperDataMock().getMockAddressList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/address/ie/D02X285?lines=3"))
		.andExpect(jsonPath("$[0].addressline1", is(address.getAddressline1())))
		.andExpect(status().isOk());
	}	
	
	@Test
	public void xmlQueryFormatAddressGeoTest() throws Exception {
		Address address = new HelperDataMock().getMockAddressList().get(0);	
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.ADDRESS_GEO))).thenReturn(new HelperDataMock().getMockAddressGeoList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/addressgeo/ie/D02X285?lines=3&format=xml"))
		.andExpect(xpath("/Addresses/Address[1]/addressline1").string(address.getAddressline1()))
		.andExpect(status().isOk());
	}
		
	@Test
	public void xmlQueryFormatPositionTest() throws Exception {		
		Address address = new HelperDataMock().getMockPositionList().get(0);
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.POSITION))).thenReturn(new HelperDataMock().getMockPositionList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/position/ie/D02X285?lines=3&format=xml"))
		.andExpect(xpath("/Addresses/Address/latitude").string(address.getLatitude()))
		.andExpect(xpath("/Addresses/Address/longitude").string(address.getLongitude()))
		.andExpect(status().isOk());
	}	

	@Test
	public void xmlQueryFormatReverseGeoTest() throws Exception {
		Address address = new HelperDataMock().getMockAddressList().get(0);
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaReverseGeo())).thenReturn(new HelperDataMock().getMockAddressList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/rgeo/ie/53.332067/-6.255492?distance=50&format=xml"))
		.andExpect(xpath("/Addresses/Address[1]/addressline1").string(address.getAddressline1()))
		.andExpect(status().isOk());
	}	
	
	@Test
	public void xmlHeaderFormatPositionTest() throws Exception {		
		Address address = new HelperDataMock().getMockPositionList().get(0);
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.POSITION))).thenReturn(new HelperDataMock().getMockPositionList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/position/ie/D02X285?lines=3").accept(MediaType.APPLICATION_XML))
		.andExpect(xpath("/Addresses/Address/latitude").string(address.getLatitude()))
		.andExpect(xpath("/Addresses/Address/longitude").string(address.getLongitude()))
		.andExpect(status().isOk());
	}
	
	@Test
	public void xmlHeaderFormatAddressTest() throws Exception {
		Address address = new HelperDataMock().getMockAddressList().get(0);	
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.ADDRESS))).thenReturn(new HelperDataMock().getMockAddressList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/address/ie/D02X285?lines=3").accept(MediaType.APPLICATION_XML))
		.andExpect(xpath("/Addresses/Address[1]/addressline1").string(address.getAddressline1()))
		.andExpect(status().isOk());
	}	
	
	@Test
	public void xmlHeaderFormatAddressGeoTest() throws Exception {
		Address address = new HelperDataMock().getMockAddressList().get(0);	
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.ADDRESS_GEO))).thenReturn(new HelperDataMock().getMockAddressGeoList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/addressgeo/ie/D02X285?lines=3").accept(MediaType.APPLICATION_XML))
		.andExpect(xpath("/Addresses/Address[1]/addressline1").string(address.getAddressline1()))
		.andExpect(status().isOk());
	}
	
	@Test
	public void xmlHeaderFormatReverseGeoTest() throws Exception {
		Address address = new HelperDataMock().getMockAddressList().get(0);
		// mock
		when(addressService.getAddresses(new HelperDataMock().getSearchCriteriaReverseGeo())).thenReturn(new HelperDataMock().getMockAddressList());
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/rgeo/ie/53.332067/-6.255492?distance=50").accept(MediaType.APPLICATION_XML))
		.andExpect(xpath("/Addresses/Address[1]/addressline1").string(address.getAddressline1()))
		.andExpect(status().isOk());
	}
	
	@Test
	public void systemNotAvailablePositionTest() throws Exception {		
		// mock
		doThrow(new SystemException(Constants.THIRD_PART_ERROR)).when(addressService).getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.POSITION));
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/position/ie/D02X285?lines=3&format=xml"))
		.andExpect(status().isServiceUnavailable());
	}
	
	@Test
	public void systemNotAvailableAddressGeoTest() throws Exception {		
		// mock
		doThrow(new SystemException(Constants.THIRD_PART_ERROR)).when(addressService).getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.ADDRESS_GEO));
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/addressgeo/ie/D02X285?lines=3&format=xml"))
		.andExpect(status().isServiceUnavailable());
	}
	
	@Test
	public void systemNotAvailableAddressTest() throws Exception {		
		// mock
		doThrow(new SystemException(Constants.THIRD_PART_ERROR)).when(addressService).getAddresses(new HelperDataMock().getSearchCriteriaWithMap(SearchType.ADDRESS));
		this.mockMvc.perform(get("/pcw/PCW45-12345-12345-1234X/address/ie/D02X285?lines=3&format=xml"))
		.andExpect(status().isServiceUnavailable());
	}
	
	@After
	public void clearup() throws Exception {
		addressRepository.deleteAll();
		searchCriteriarepository.deleteAll();
	}

}
