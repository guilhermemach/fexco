package com.fexco.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fexco.Main;
import com.fexco.entity.Address;
import com.fexco.entity.SearchCriteria;
import com.fexco.repositories.AddressRepository;
import com.fexco.repositories.SearchCriteriaRepository;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
public class AddressRepositoryTest {

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private SearchCriteriaRepository searchCriteriarepository;

	private List<Address> addressList;
	private List<SearchCriteria> criteriaList;

	@Before
	public void setup() throws Exception {
		addressList = new ArrayList<>();
		List<SearchCriteria> criteriaMockedList = new HelperDataMock().getMockSearchCriteriaList();
		this.criteriaList = searchCriteriarepository.save(criteriaMockedList);
		addressList.addAll(addressRepository.save(new HelperDataMock().getMockAddressList(criteriaMockedList.get(0))));
	}

	@Test
	public void findAllTest() throws Exception {
		List<Address> list = addressRepository.findAll();
		assertEquals(2, list.size());
	}
	
	@Test
	public void findByCriteriaTest() throws Exception {
		SearchCriteria searchCriteria = criteriaList.get(0);
		List<Address> list = addressRepository.findByCriteria(searchCriteria.getSearchType(), searchCriteria.getKey());
		assertEquals(2, list.size());
		
		SearchCriteria searchCriteria2 = criteriaList.get(1);
		List<Address> list2 = addressRepository.findByCriteria(searchCriteria2.getSearchType(), searchCriteria2.getKey());
		assertEquals(0, list2.size());	
	}
		
	@Test
	public void saveTest() throws Exception {
		SearchCriteria searchCriteria = this.criteriaList.get(0);
		Address addressToBeSaved= new HelperDataMock().getMockAddressList(searchCriteria).get(0);
		Address response = addressRepository.save(addressToBeSaved);
		assertTrue(response.getId() > 0);
	}

	@After
	public void clearup() throws Exception {
		addressRepository.deleteAll();
		searchCriteriarepository.deleteAll();
	}

}
