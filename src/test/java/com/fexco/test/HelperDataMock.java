package com.fexco.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;

import com.fexco.constant.SearchType;
import com.fexco.entity.Address;
import com.fexco.entity.SearchCriteria;
import com.fexco.util.Util;

/**
 * Mock which hold mock data for tests.
 * 
 * @author guimachado
 *
 */
@Ignore
public class HelperDataMock {
	
	protected List<SearchCriteria> getMockSearchCriteriaList() throws Exception {
		List<SearchCriteria> finalList = new ArrayList<SearchCriteria>();
		SearchCriteria  s1 = new SearchCriteria("A", "B",  SearchType.ADDRESS);
		s1.setLines("1");
		s1.setKey(Util.md5(s1.toString()));
		
		SearchCriteria  s2 = new SearchCriteria("B", "C", SearchType.ADDRESS_GEO);		
		s2.setKey(Util.md5(s2.toString()));
		s2.setLines("3");
		
		finalList.add(s1);
		finalList.add(s2);
		return finalList;
	}
	
	protected List<Address> getMockAddressList(SearchCriteria searchCriteria) {
		List<Address> finalList = getMockAddressList();
		finalList.forEach((address) -> address.setCriteria(searchCriteria) ); 
		return finalList;
	}
	
	protected List<Address> getMockAddressGeoList() {
		List<Address> finalList = getMockAddressList();
		finalList.forEach((address) ->  {			
			address.setLatitude("53.332067");
			address.setLongitude("-6.255492");}); 
		return finalList;
	}
	
	protected List<Address> getMockPositionList() {
		List<Address> finalList = new ArrayList<Address>();
		Address address1= new Address();
		address1.setLatitude("53.332067");
		address1.setLongitude("-6.255492");		
		finalList.add(address1);
	
		return finalList;
	}
	
	protected List<Address> getMockAddressList() {
		List<Address> finalList = new ArrayList<Address>();
		Address address1= new Address();
		address1.setAddressline1("Department of Communications, Climate Action and Environment");
		address1.setSummaryline("Department of Communications, Climate Action and Environment, 29-31 Adelaide Road, Dublin 2, D02 X285");
		address1.setOrganisation("Department of Communications, Climate Action and Environment");
		address1.setNumber("29-31");
		address1.setPremise("29-31");
		address1.setStreet("Adelaide Road");
		address1.setPostcode("D02 X285");
		
		Address address2= new Address();
		address2.setAddressline1("Department of Communications, Climate Action and Environment");
		address2.setSummaryline("Department of Communications, Climate Action and Environment, 29-31 Adelaide Road, Dublin 2, D02 X285");
		address2.setOrganisation("Department of Communications, Climate Action and Environment");
		address2.setNumber("29-31");
		address2.setPremise("29-31");
		address2.setStreet("Adelaide Road");
		address2.setPostcode("D01 X285");
		
		finalList.add(address1);
		finalList.add(address2);
		return finalList;
	}
	
	public SearchCriteria getSearchCriteriaWithMap(SearchType s) throws Exception{
		SearchCriteria searchCriteria = new SearchCriteria("PCW45-12345-12345-1234X", "D02X285",  s);
		searchCriteria.setLines("3");
		Map<String, String> list = new HashMap<>();
		list.put("format", "xml");
		list.put("lines", "3");
		searchCriteria.setMap(list);
		searchCriteria.setKey(Util.md5(searchCriteria.toString()));
		return searchCriteria;
	}	
	
	public SearchCriteria getSearchCriteriaWithKey(SearchType s) throws Exception{
		SearchCriteria searchCriteria = getSearchCriteriaWithMap(s);
		searchCriteria.setKey(Util.md5(searchCriteria.toString()));
		return searchCriteria;
	}
	
	public SearchCriteria getSearchCriteriaReverseGeo() throws Exception{
		SearchCriteria searchCriteria = new SearchCriteria("PCW45-12345-12345-1234X", null, SearchType.REVERSE_GEO, "53.332067", "-6.255492", "50");
		Map<String, String> list = new HashMap<>();
		list.put("format", "xml");
		list.put("distance", "50");
		searchCriteria.setMap(list);
		searchCriteria.setKey(Util.md5(searchCriteria.toString()));
		return searchCriteria;
	}
}	
