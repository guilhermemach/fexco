# README #


### Test Fexco ###

# Summary

The application is a standalone solution for purpose of tests (using spring boot) with an embbeded database.
In case of a prodution deploy is necessary to make some changes to a final database solution.

# Application Libraries/Framework

* Java8
* Maven3.3
* Spring Boot with Spring MVC
* Retrofit2
* Hibernate/JPA
* Mockito / MockMvc

# Tools

* Eclipse
* RestClient (http rest client)

# Deploy / Run

Necessary to have Maven to install lib dependencies e run the project.

```
#!linux

$ mvn install
$ mvn test

```


Deployment instructions


```
#!linux

$ mvn spring-boot:run

```


# HTTP Queries

* http://localhost:8080/pcw/PCW45-12345-12345-1234X/address/ie/D02X285

* http://localhost:8080/pcw/PCW45-12345-12345-1234X/position/ie/D02X285

* http://localhost:8080/pcw/PCW45-12345-12345-1234X/addressgeo/ie/D02X285

* http://localhost:8080/pcw/PCW45-12345-12345-1234X/rgeo/ie/53.332067/-6.255492?distance=50&format=xml&lines=3